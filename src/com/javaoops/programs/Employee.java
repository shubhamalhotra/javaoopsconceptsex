package com.javaoops.programs;

public class Employee {

    private String name;
    private int id;
    private long salary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }


    public void setName(String name)
    {
        this.name = name;
    }
    public String getName() {
        return name;
    }

}
